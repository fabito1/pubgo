# Fabito's golang modules

Public ONLY golang modules

## Dependencies

- golang: e.g. `apt install golang`
- gotest: `go get -u github.com/rakyll/gotest`

## Import in Golang Modules

This is as simple as it is generally in golang: like `import "fmt"` do `import "gitlab.com/fabito1/pubgo/greetings"`.
