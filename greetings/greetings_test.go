package greetings

import (
    "encoding/csv"
    "log"
    "os"
    "regexp"
    "testing"
    "github.com/stretchr/testify/assert"
)

type TestDataItem struct{
    desc string
    name string
    result string
    err string
}

func TestHello(t *testing.T)  {
    var tables []TestDataItem
    /*
    // Set test data inline
    tables = []TestDataItem{
        {
            "no name should throw error",
            "",
            "",
            "empty name",
        },
        {
            "any name should result in welcome string",
            "Fabian",
            "Fabian",
            "",
        },
    }
    // log.Println(tables)
    */

    // Read test data from csv file
    file, err := os.Open("./testdata/hello.csv")
    if err != nil {
        log.Fatal(err)
    }
    reader := csv.NewReader(file)
    reader.Comma = ';'
    records, err := reader.ReadAll()
    if err != nil {
        log.Fatal(err)
    }
    for _, r := range records[1:] {
        tables = append(tables, TestDataItem{r[0], r[1], r[2], r[3]})
    }
    // log.Println(tables)

    assert := assert.New(t)  // optional to omit t's
    for _, table := range tables {
        result, err := Hello(table.name)
        if err == nil {
            want := regexp.MustCompile(`\b`+table.result+`\b`)
            assert.Regexp(want, result, table.desc)
            assert.Equal(table.err, "", table.desc)
        } else {
            assert.Equal(table.result, result)
            assert.Equal(table.err, err.Error(), table.desc)
        }
    }
}

func TestHellos(t *testing.T)  {
    tables := []struct{
        desc string
        names []string
        err string
    }{
        {
            "no names should return nothing",
            []string{},
            "",
        },
        {
            "empty name in names should throw error",
            []string{"Fabian", ""},
            "empty name",
        },
        {
            "any names should return matching messages",
            []string{"Fabian", "Anna", "Max"},
            "",
        },
    }
    assert := assert.New(t)
    for _, table := range tables {
        msgs, err := Hellos(table.names)
        if err == nil {
            assert.NotNil(msgs)
            assert.Equal(len(msgs), len(table.names))
            for name, msg := range msgs {
                want := regexp.MustCompile(`\b`+name+`\b`)
                assert.Regexp(want, msg, table.desc)
            }
        } else {
            assert.Nil(msgs)
            assert.Equal(err.Error(), table.err)
        }
    }
}
