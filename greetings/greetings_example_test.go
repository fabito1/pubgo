package greetings_test

import (
    "fmt"
    "regexp"
    "gitlab.com/fabito1/pubgo/greetings"
)

func ExampleHello() {
    msg, _ := greetings.Hello("Fabian")
    want := regexp.MustCompile(`\bFabian\b`)
    fmt.Println(want.MatchString(msg))
    // Output: true
}

func ExampleHellos() {
    names := []string{"Fabian", "Anna", "Max"}
    msgs, _ := greetings.Hellos(names)
    for n, msg := range msgs {
        for _, name := range names {
            if name == n {
                want := regexp.MustCompile(`\b`+name+`\b`)
                fmt.Println(want.MatchString(msg))
                break
            }
        }
    }
    // Output:
    // true
    // true
    // true
}
