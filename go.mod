module gitlab.com/fabito1/pubgo

go 1.15

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/rakyll/gotest v0.0.5 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
